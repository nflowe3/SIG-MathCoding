## Log

*   **Week 3:** Used SAGE to verify e^(ix) = cos(x) + i*sin(x), the sin series expansion of f(x) = x, and the solution of f'' + f = x.  
    Pinned time down to 1pm. Will henceforth meet in CS Lounge, on the couches, due to low volume.
*   **Week 5:** Discussed potential projects in R and Prolog.  
*   **Week 7:** Prolog: incorporate verbnet, generate sentence stories.  
    R: simulate interaction dynamics, create shared folder (~~Dropbox?~~).  
*   **Week 8:** Introduced SAGE (ex: rose curves) and Prolog (ex: formal grammar) to new students.  
    Indicated further online resources for R and NetLogo.
*   **Week 9:** Split Prolog project into sentence parser and VerbNet interface. Discussed minimalistic grammars and online resources.

## Agenda

*   **Week 3:** using power series to compute cos + i sin (i.e. Euler's formula); investigate SAGE interacts  
    ~~functional vs. logic programming~~  
*   **Week 4:** off  
*   **Week 5:** interactive R plots via SageMathCell and Jupyter; investigate R + D3  
    ~~introduce logic programming, optimization; investigate TBD~~  
*   **Week 6:** off  
*   **Week 7:** Scope projects: currently, these are analysis of a complex dynamical system, and semantic awareness.  
*   **Week 8:** (meet normally)
*   **Week 9:** Follow up on projects. Discuss new members' interests and scope their projects.
*   **Week 10:** (meet normally)

## Targets

**SAGEMath:** symbolic calculus & group theory, interactive plots  
**VerbNet:** pragmatic knowledge for syntactic agreement

**matplotlib:** parametric and implicit plots & animations  
**ipython / D3:** browser-based interactive widgets

**Transcendental Functions** (the precise relationship of exp, cos, sin)  
**Dynamical Systems** (population dynamics, etc.)

~~**Laplace Transform** (decompose analytic functions into growth & periodic components)  
**Matrix Decomposition** (automate solution of Ax = b by Gaussian elimination)  
**Linear Optimization** (maximize utility under constraints)~~

...and the rigorous basis of other common engineering techniques. The tools are also good for number theory, game theory, graph theory, optimization, and other cool things we may explore.

I am liable to assume basic knowledge of functional (higher-order) programming (e.g. Pythonic for comprehensions, R vectorized functions), but newcomers will be pointed in the right direction.

## Resources

### Prolog for logic programming

[Two books on idioms and data structures pending.]

SWI-Prolog's [SWISH](http://swish.swi-prolog.org/#tabbed-tab-2), with [online companion book](http://www.learnprolognow.org/lpnpage.php?pageid=online).

### Java Ecosystem

##### functional programming

[Clojure. Scala.]

##### individual-based modelling

[NetLogo](https://ccl.northwestern.edu/netlogo/) official site, with [online examples](http://www.netlogoweb.org/launch#http://www.netlogoweb.org/assets/modelslib/Sample%20Models/Biology/Rabbits%20Grass%20Weeds.nlogo).

### Data

[VerbNet. Perseus.]

### SAGEMath

##### running

[SAGEMathCell](https://sagecell.sagemath.org/) for quick computations. [SAGEMathCloud](https://cloud.sagemath.com/) for full projects.

Consult [main site](http://www.sagemath.org/) for local installs, which we will cover the first week.

##### learning

Prof. Verschelde's [MCS 320 course page](http://homepages.math.uic.edu/~jan/mcs320/), with [extensive notes](http://homepages.math.uic.edu/~jan/mcs320/mcs320notes/lec01.html) we will reference.

Sage Wiki's [interactive examples](https://wiki.sagemath.org/interact). ~~Find [working](https://wiki.sagemath.org/interact/calculus?action=recall&rev=51) [code](https://wiki.sagemath.org/interact/number_theory?action=recall&rev=13) in pages' revision history (under 'Info').~~

</div>