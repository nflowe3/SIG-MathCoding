% Phrase Structure Rules
s --> np(subject), vp.

np(_) --> det, n.
np(_) --> det, n, pp.
np(X) --> pro(X).

vp --> v.
vp --> v, np(object).
vp --> v, np(object), pp.

pp --> p, np.

pro(subject) --> [i].
pro(subject) --> [you].
pro(subject) --> [he].
pro(subject) --> [she].
pro(subject) --> [it].
pro(subject) --> [we].
pro(subject) --> [they].

pro(object)  --> [me].
pro(object)  --> [you].
pro(object)  --> [him].
pro(object)  --> [her].
pro(object)  --> [us].
pro(object)  --> [them].

% Lexical Enumeration
det --> [the].
det --> [a].
det --> [some].
det --> [many].
det --> [any].
det --> [this].
det --> [that].
det --> [what].
det --> [which].

n --> [boy].
n --> [girl].
n --> [man].
n --> [woman].
n --> [wizard].
n --> [witch].
n --> [bread].
n --> [soup].
n --> [game].

v --> [eats].
v --> [drinks].
v --> [curses].
v --> [hates].
v --> [loves].
v --> [likes].
v --> [plays].

p --> [with].
p --> [on].
p --> [before].
p --> [for].