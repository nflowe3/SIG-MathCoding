% to list one-by-one:
% s(X). ; ; ; ; % ...

% for graphical debugger:
% tspy(s). s(X).

% TODO: quality metric

s(S) :-
    S = (NP, VP),
    np(NP), vp(VP).
%    NP = you, vp(VP). % or, cut after the NP.

np(NP) :- nps(NP).
np(NP) :-
    NP = (DET, N, CONJ, VP),
    det(DET), n(N), conj(CONJ), vps(VP).
% simple version bans conjugation.
nps(NP) :-
    NP = (DET, N),
    det(DET), n(N).

vp(VP) :- vps(VP).
vp(VP) :-
    VP = (V, NP),
    v(V), np(NP).
% simple version bans conjugation.
vps(VP) :-
    VP = (V, NP),
    v(V), nps(NP).

% VERIFY efficiency.

simple(NP) :-
    np(NP),
    NP = (DET, N).
    % WIP


n(owl).
n(ram).
v(saw).
v(feared).
v(sought).

det(the).
det(some).
% 'a/an' awaits handling in postproc.
conj(which).
