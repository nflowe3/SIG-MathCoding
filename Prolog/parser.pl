% This is a parsing program using both bottom up and
% top down parsing strategies with a DCG 

% Author: Nigel Flower

:- op(700,xfx,--->).

%% ---------------------- Bottom Up Recognition --------------------
recognize_bottomup([s]).

% This function recursively builds up the sentence left to right	
recognize_bottomup(String) :-

	nl,write('STATE: '),write(String),
        split(String,Front,Middle,Back),
	( Cat ---> Middle 
         ;
	  (Middle = [Word], lex(Word,Cat))
    ),

	tab(5),write('RULE: '),write((Cat ---> Middle)),nl,
	append(Front,[Cat|Back],NewString),
	recognize_bottomup(NewString).
 
% This function splits a list into three segments. It is used above
% to separate the sections of the CFG rules
split(ABC, A, B, C) :-
	append(A, BC, ABC),
	append(B, C, BC).

%% ---------------------- Top Down Recognition --------------------


% Expand category by lexical rule
recognize_topdown(Category,[Word|Reststring],Reststring) :-
	nl, write('String to recognize: '), write([Word|Reststring]), 
	nl, write('Category to recognize: '), write(Category),
	lex(Word,Category).

% Expand category through phrase structure rule
recognize_topdown(Category,String,Reststring) :-
        Category ---> RHS,
        nl, write('Rule: '), write((Category ---> RHS)),
        matches(RHS,String,Reststring).

%%% matches(+symbols, +sentence, ?rest of sentence)
%%% Checks whether the list of symbols can be expanded into terminal
%%% symbols that match whats next on the input string.

%%% The list of symbols is empty. -> Return the string unchanged.
matches([],String,String).

%%% The list of symbols was produced by a phrase structure rule and
%%% therefore contains category symbols. Try to recognize a substring
%%% matching the first category on the list, then try to match the
%%% rest of the categories.
matches([Category|Categories],String,RestString) :-
        recognize_topdown(Category,String,String1),
        matches(Categories,String1,RestString).

recognize_topdown(String) :-
        recognize_topdown(s,String,[]).

%% ---------------------- Top Down Parsing --------------------

% Expand category by lexical rule
parse_topdown(Category,[Word|Reststring],Reststring,[Category,Word]) :-
	nl, write('String to recognize: '), write([Word|Reststring]), 
	nl, write('Category to recognize: '), write(Category),
	lex(Word,Category).
 
parse_topdown(Category,String,Reststring,[Category|Subtrees]) :-
        Category ---> RHS,
        nl, write('Rule: '), write((Category ---> RHS)),
        parse_matches(RHS,String,Reststring,Subtrees).

% The list of symbols is empty. -> Return the string unchanged.
parse_matches([],String,String,[]).

% The list of symbols was produced by a phrase structure rule and
% therefore contains category symbols. Try to recognize a substring
% matching the first category on the list, then try to match the
% rest of the categories.
parse_matches([Category|Categories],String,RestString,[Subtree|Subtrees]) :-
        parse_topdown(Category,String,String1,Subtree),
        parse_matches(Categories,String1,RestString,Subtrees).

parse_topdown(String,Parse) :-
        parse_topdown(s,String,[],Parse).
